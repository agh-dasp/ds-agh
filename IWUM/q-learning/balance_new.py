import math
import numpy as np
import gym


class QLearner:
    def __init__(self):
        self.environment = gym.make('CartPole-v1', render_mode="human")
        self.attempt_no = 1
        self.upper_bounds = [
            self.environment.observation_space.high[0],
            0.5,
            self.environment.observation_space.high[2],
            math.radians(50)
        ]
        self.lower_bounds = [
            self.environment.observation_space.low[0],
            -0.5,
            self.environment.observation_space.low[2],
            -math.radians(50)
        ]
        print(self.lower_bounds,self.upper_bounds)
        self.gamma = 1
        self.a_gamma = .992
        self.knowledge = dict()
        self.alpha = .1
        self.discount_factor = .25

    def learn(self, max_attempts):
        for _ in range(max_attempts):
            reward_sum = self.attempt()
            print(reward_sum)
        self.gamma *= self.a_gamma

    def attempt(self):
        observation = self.discretise(self.environment.reset()[0])
        done = False
        reward_sum = 0.0
        while not done:
            self.environment.render()
            action = self.pick_action(observation)
            new_observation, reward, done, _, info = self.environment.step(action)
            new_observation = self.discretise(new_observation)
            self.update_knowledge(action, observation, new_observation, reward)
            observation = new_observation
            reward_sum += reward
        self.attempt_no += 1
        return reward_sum

    def discretise(self, observation):
        return np.digitize(observation/self.upper_bounds,np.linspace(-1,1,20))
        #return 1, 1, 1, 1

    def pick_action(self, observation):
        if np.random.rand(1) > self.gamma:
            observation = tuple(observation)
            if observation in self.knowledge:
                return np.argmax(self.knowledge[observation])
        
        return self.environment.action_space.sample()

    def update_knowledge(self, action, observation, new_observation, reward):
        observation = tuple(observation)
        new_observation = tuple(new_observation)
        if new_observation not in self.knowledge:
            self.knowledge[new_observation] = [0,0]
        if observation in self.knowledge:
            self.knowledge[observation][action] = (1-self.alpha)*self.knowledge[observation][action] +\
                                                  self.alpha*(reward + self.discount_factor*np.max(self.knowledge[new_observation]))
        else:
            self.knowledge[observation] = [0,0]
            self.knowledge[observation][action] = reward


def main():
    learner = QLearner()
    learner.learn(10000)


if __name__ == '__main__':
    main()
