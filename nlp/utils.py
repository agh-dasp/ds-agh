import os
import re
import requests as r

def read_current_pwd():
    return os.popen('pwd').read()

def findall(pattern, directory = '.', limit=None):
    out = dict()
    for i in os.listdir(directory)[:limit]:
        with open(os.path.join(directory,i), 'r') as f:
            x = re.findall(pattern, f.read(),re.IGNORECASE)
            out[i] = x
    return out

def config_es(es_url):
    response = r.put(
        url=f"{es_url}/acts",
        json={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "polish-law-analyzer": {
                            "type": "custom",
                            "tokenizer": "standard",
                            "filter": [
                                "synonym-filter",
                                "morfologik_stem",
                                "lowercase"
                            ]
                        }
                    },
                    "filter": {
                        "synonym-filter": {
                            "type": "synonym",
                            "synonyms": [
                                "kpk => kodeks postępowania karnego",
                                "kpc => kodeks postępowania cywilnego",
                                "kk => kodeks karny",
                                "kc => kodeks cywilny"
                            ]
                        }
                    }
                }
            }
        }
    )
    return response

def config_es_index(es_url):
    response = r.put(
        url=f"{es_url}/acts/_mapping",
        json={
            "properties": {
                "content": {
                    "type": "text",
                    "analyzer": "polish-law-analyzer"
                }
            }
        }
    )
    return response
    
